class Game

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  attr_reader :secret_code

  def get_guess
    guess = @stdin.to_s #switch to guess = gets.chomp for player version
    #guess = gets.chomp
    Code.parse(guess)
  end

  def display_matches(guess)
    puts "near matches: #{secret_code.near_matches(guess)}"
    puts "exact matches: #{secret_code.exact_matches(guess)}"
  end

  def play
    guess = get_guess
    turns = 10
    until secret_code.exact_matches(guess) == 4
      display_matches(guess)
      guess = get_guess
      turns -= 1
      if turns == 0
        puts "Out of moves!"
        raise
      end
    end
    puts "You won!"
  end

end

class Code

  PEGS = {"R" => "Red", "G" => "Green", "B" => "Blue", "Y" => "Yellow", "O" => "Orange",
    "P" => "Purple"}

  def initialize(arr)
    @pegs = arr
  end

  attr_reader :pegs

  def [](num)
    pegs[num]
  end

  def exact_matches(code_guess)
    hits = 0
    pegs.each_with_index do |peg, idx|
      hits += 1 if code_guess[idx] == peg
    end
    hits
  end

  def near_matches(code_guess)
    hits = 0
    pegs.each_with_index do |peg, idx|
      if code_guess[idx] != peg
        code_guess.pegs.each_with_index do |guess_peg, idx2|
          if guess_peg == peg && pegs[idx2] != guess_peg
            hits += 1
            code_guess.pegs.delete_at(idx2)
          end
        end
      end
    end

    hits
  end

  def ==(code_guess)
    exact_matches(code_guess) == 4
  end

  def self.parse(str)
    arr = str.upcase.split("")
    raise if arr.any? {|el| PEGS.include?(el) == false}
    Code.new(arr)
  end

  def self.random
    random_code = []
    4.times { random_code << PEGS.keys.sample }
    Code.new(random_code)
  end

end
#
# game = Game.new
# game.play
